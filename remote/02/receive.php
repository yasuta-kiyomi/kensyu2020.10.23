<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>第二回課題、フォーム部品練習</title>
  </head>
  <body>
    こんにちは
    <?php
      echo $_GET['user_name'];
     ?>
    さん！<br>
    あなたの住所は
    <?php
      echo $_GET['address'];
     ?>
    ですね。<br>
    また、性別は
    <?php
      echo $_GET['category2'];
     ?>
    で、年齢は
    <?php
     if(isset($_GET["age"])) {
       $fruit = $_GET["age"];
       echo $fruit;
     }
     ?>
    ですね。<br>
    そして趣味は
    <?php
      foreach ($_GET["hobby"] as $value) {
        echo "{$value}、";
      }
     ?>
     ですね。<br>
    「
    <?php
      echo $_GET['note'];
     ?>
    」
  </body>
</html>
