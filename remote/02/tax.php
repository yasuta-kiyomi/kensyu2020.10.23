<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>消費税計算ページ</title>
  </head>
  <body>
    <h2>消費税計算ページ</h2>
    <form method='post' action='tax.php'>
      <h3>「計算」</h3>
      <table border="1" style="border-collapse:collapse;">
        <tr>
          <th>商品名</th>
          <th>価格（単位：円、税抜き）</th>
          <th>個数</th>
          <th>税率</th>
        </tr>
        <tr>
          <td><input type="text" name="shohin_name01"></td>
          <td><input type="text" name="price01">円</td>
          <td><input type="number" name="quantity01" min="0" value="0" step="1">個</td>
          <td><input type="radio" name="tax_rate01" value="8" checked>8%<input type="radio" name="tax_rate01" value="10">10%</td>
        </tr>
        <tr>
          <td><input type="text" name="shohin_name02"></td>
          <td><input type="text" name="price02">円</td>
          <td><input type="number" name="quantity02" min="0" value="0" step="1">個</td>
          <td><input type="radio" name="tax_rate02" value="8" checked>8%<input type="radio" name="tax_rate02" value="10">10%</td>
        </tr>
        <tr>
          <td><input type="text" name="shohin_name03"></td>
          <td><input type="text" name="price03">円</td>
          <td><input type="number" name="quantity03" min="0" value="0" step="1">個</td>
          <td><input type="radio" name="tax_rate03" value="8" checked>8%<input type="radio" name="tax_rate03" value="10">10%</td>
        </tr>
        <tr>
          <td><input type="text" name="shohin_name04"></td>
          <td><input type="text" name="price04">円</td>
          <td><input type="number" name="quantity04" min="0" value="0" step="1">個</td>
          <td><input type="radio" name="tax_rate04" value="8" checked>8%<input type="radio" name="tax_rate04" value="10">10%</td>
        </tr>
        <tr>
          <td><input type="text" name="shohin_name05"></td>
          <td><input type="text" name="price05">円</td>
          <td><input type="number" name="quantity05" min="0" value="0" step="1">個</td>
          <td><input type="radio" name="tax_rate05" value="8" checked>8%<input type="radio" name="tax_rate05" value="10">10%</td>
        </tr>
      </table>
      <input type="submit" value="結果">
      <input type="reset" value="クリア">
      <h3>「結果」</h3>
      <table border="1" style="border-collapse:collapse;">
        <tr>
          <th>商品名</th>
          <th>価格（単位:円、税抜き）</th>
          <th>個数</th>
          <th>税率</th>
          <th>小計（単位:円）</th>
        </tr>
        <tr>
          <td><?php echo $_POST['shohin_name01']; ?></td>
          <td><?php echo $_POST['price01']; ?></td>
          <td><?php echo $_POST['quantity01']; ?></td>
          <td><?php echo $_POST['tax_rate01']; ?>%</td>
          <td>
              <?php
                $tax01 = $_POST['tax_rate01'] / 100 + 1;
                $subtotal01 = $_POST['price01'] * $_POST['quantity01'] * $tax01 ;
                echo $subtotal01 . "円（税込み）";
               ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $_POST['shohin_name02']; ?></td>
          <td><?php echo $_POST['price02']; ?></td>
          <td><?php echo $_POST['quantity02']; ?></td>
          <td><?php echo $_POST['tax_rate02']; ?>%</td>
          <td>
              <?php
                $tax02 = $_POST['tax_rate02'] / 100 + 1;
                $subtotal02 = $_POST['price02'] * $_POST['quantity02'] * $tax02 ;
                echo $subtotal02 . "円（税込み）";
               ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $_POST['shohin_name03']; ?></td>
          <td><?php echo $_POST['price03']; ?></td>
          <td><?php echo $_POST['quantity03']; ?></td>
          <td><?php echo $_POST['tax_rate03']; ?>%</td>
          <td>
              <?php
                $tax03 = $_POST['tax_rate03'] / 100 + 1;
                $subtotal03 = $_POST['price03'] * $_POST['quantity03'] * $tax03 ;
                echo $subtotal03 . "円（税込み）";
               ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $_POST['shohin_name04']; ?></td>
          <td><?php echo $_POST['price04']; ?></td>
          <td><?php echo $_POST['quantity04']; ?></td>
          <td><?php echo $_POST['tax_rate04']; ?>%</td>
          <td>
              <?php
                $tax04 = $_POST['tax_rate04'] / 100 + 1;
                $subtotal04 = $_POST['price04'] * $_POST['quantity04'] * $tax04 ;
                echo $subtotal04 . "円（税込み）";
               ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $_POST['shohin_name05']; ?></td>
          <td><?php echo $_POST['price05']; ?></td>
          <td><?php echo $_POST['quantity05']; ?></td>
          <td><?php echo $_POST['tax_rate05']; ?>%</td>
          <td>
              <?php
                $tax05 = $_POST['tax_rate05'] / 100 + 1;
                $subtotal05 = $_POST['price05'] * $_POST['quantity05'] * $tax05 ;
                echo $subtotal05 . "円（税込み）";
               ?>
          </td>
        </tr>
        <tr>
          <td colspan="4">合計</td>
          <td>
              <?php
                $total = $subtotal01 + $subtotal02 + $subtotal03 + $subtotal04 + $subtotal05;
                echo $total . "円";
               ?>
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>
