<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>行数と列数を指定してテーブル作成</title>
  </head>
  <body>
    <h1>テーブル作成03</h1>
    <h2>「入力」</h2>
    <form method='get' action='./loop03.php'>
      <input type="number" name="line01" min="0" value="0" step="1">行 × <input type="number" name="line02" min="0" value="0" step="1">列<br>
      <input type="submit" value="作成">
      <input type="reset" value="クリア">
    </form>
    <h2>「作成結果」</h2>
    <table border="1" style="border-collapse:collapse;">
      <?php
        for($i=1; $i <= $_GET['line01']; $i++){
          echo "<tr>";
          for ($j=1; $j <= $_GET['line02']; $j++) {
            echo "<td>$i - $j</td>";
            // echo "<td>" . $i . "-" . $j . "</td>";  のほうがよき
          }
          echo "</tr>";
        }
      ?>
    </table>
  </body>
</html>
