<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>行数と列数を指定してテーブル作成</title>
  </head>
  <body>
    <h1>テーブル作成02</h1>
    <h2>「入力」</h2>
    <form method='get' action='./loop02.php'>
      <input type="number" name="line01" min="0" value="0" step="1">行 × <input type="number" name="line02" min="0" value="0" step="1">列<br>
      <input type="submit" value="作成">
      <input type="reset" value="クリア">
    </form>
    <h2>「作成結果」</h2>
    <table border="1" style="border-collapse:collapse;">
      <?php
        for($i=0; $i < $_GET['line01']; $i++){
          echo "<tr>";
          for ($j=0; $j < $_GET['line02']; $j++) {
            echo "<td>できた</td>";
          }
          echo "</tr>";
        }
      ?>
    </table>
  </body>
</html>
