<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>ログイン画面_安田02</title>
  </head>
  <body>
    <h1>ログイン画面</h1>
    <h2>以下の表に入力してください。</h2>
    <form method='post' action='./result02.php'>
      <table border="1" style="border-collapse:collapse;">
        <tr>
          <th>ログインID</th>
          <td><input type="text" name="login" placeholder="半角英数字"></td>
        </tr>
        <tr>
          <th>パスワード</th>
          <td><input type="password" name="password"></td>
        </tr>
      </table>
      <input type="submit" name="send" value="ログイン">
    </form>
  </body>
</html>
