<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>指定行数でテーブル作成</title>
  </head>
  <body>
    <h1>テーブル作成01</h1>
    <h2>「入力」</h2>
    <form method='post' action='./loop01.php'>
      <input type="number" name="line" min="0" value="0" step="1">行のテーブルを作成する<br>
      <input type="submit" value="作成">
      <input type="reset" value="クリア">
    </form>
    <h2>「作成結果」</h2>
    <table border="1" style="border-collapse:collapse;">
      <?php
        for($i=0; $i < $_POST['line']; $i++){
          echo "<tr><td>なるほど</td><td>ここが中身か</td><td>把握</td></tr>";
        }
      ?>
    </table>
  </body>
</html>
