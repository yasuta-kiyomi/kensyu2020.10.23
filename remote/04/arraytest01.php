<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>配列01</title>
  </head>
  <body>
    <?php
      $fruit = array("りんご", "すいか", "みかん", "なし", "イチゴ", "かき");

      echo "配列3つ目は「" . $fruit[3] . "」で、1つ目は「" . $fruit[1] . "」です！" . "<br>";

      echo $fruit[0] . "<br>";

      echo $fruit[5] . "<br>";

      $fruit[2] = "いちじく"; // 上書きされる

      $fruit[6] = "キウイ"; // 追加される

      var_dump($fruit); // 配列の中身がすべて表示される

      echo "<br/>";

      echo "<table border='1'>";
      for ($i=0; $i < count($fruit) ; $i++) {
        echo "<tr><td>" . $fruit[$i] . "</td></tr>";
      }
      echo "</table>";

      echo "配列の個数は" . count($fruit) . "です！";

      echo "<hr>";
      foreach ($fruit as $each) {
        echo $each . "<br/>";
      }
      echo "<hr>";

      foreach ($fruit as $key => $value) {
        echo $key . "番目の要素は「" . $value . "」です。<br/>";
      }

      $needle = "みかん";
      if (in_array($needle, $fruit)) {
        echo $needle . "がfruitの要素に存在しています。";
      }else {
        echo $needle . "がfruitの要素に存在しません。";
      }
    ?>
    <!-- ブラウザで表示させるときは<pre>で囲むと見やすく表示される -->
    <pre>
      <?php var_dump($fruit); ?>
    </pre>
  </body>
</html>
