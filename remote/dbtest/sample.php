<!DOCTYPE html>
<?php
  $DB_DSN = "mysql:host=localhost; dbname=kyasuta; charset=utf8";
  $DB_USER = "webaccess";
  $DB_PW = "toMeu4rH";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

  $query_str = "SELECT *
                FROM testtable01";

  echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();
?>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>データベース</title>
  </head>
  <body>
    <pre>
      <?php
        var_dump($result);
      ?>
    </pre>
    <table border="1">
      <tr>
        <th>料理名</th>
        <th>価格</th>
        <th>メモ</th>
      </tr>
      <?php
        foreach ($result as $each) {
          echo
            "<tr>" .
              "<td>" . $each['dishname'] . "</td>" .
              "<td>" . $each['price'] . "円</td>" .
              "<td>" . $each['memo'] . "</td>" .
            "</tr>";
        }
      ?>
    </table>
  </body>
</html>
