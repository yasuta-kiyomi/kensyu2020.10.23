<!DOCTYPE html>
<?php
  include("./include/statics.php");
  include("./include/functions.php");

  $pdo = commonDB();

  $syain_name = "";
  if(isset($_POST['syain_name']) AND $_POST['syain_name'] != ""){
     $syain_name = $_POST['syain_name'];
  }else{
    commonError();
  }

  $syain_pref = "";
  if(isset($_POST['pref']) AND $_POST['pref'] != ""){
     $syain_pref = $_POST['pref'];
  }else{
    commonError();
  }

  $syain_seibetu = "";
  if(isset($_POST['up_seibetu']) AND $_POST['up_seibetu'] != ""){
     $syain_seibetu = $_POST['up_seibetu'];
  }else{
    commonError();
  }

  $syain_age = "";
  if(isset($_POST['age']) AND $_POST['age'] != "" AND is_numeric($_POST['age'])){
     $syain_age = $_POST['age'];
  }else{
    commonError();
  }

  $syain_busyo = "";
  if(isset($_POST['up_busyo']) AND $_POST['up_busyo'] != ""){
     $syain_busyo = $_POST['up_busyo'];
  }else{
    commonError();
  }

  $syain_yakusyoku = "";
  if(isset($_POST['up_yakusyoku']) AND $_POST['up_yakusyoku'] != ""){
     $syain_yakusyoku = $_POST['up_yakusyoku'];
  }else{
    commonError();
  }

  $syain_id = "";
  if(isset($_POST['member_ID']) AND $_POST['member_ID'] != ""){
     $syain_id = $_POST['member_ID'];
  }else{
    commonError();
  }

  $query_str = "UPDATE member AS m
                SET m.name = '" . $syain_name . "',
                    m.pref = '" . $syain_pref . "',
                    m.seibetu = '" . $syain_seibetu . "',
                    m.age = '" . $syain_age . "',
                    m.section_ID = '" . $syain_busyo . "',
                    m.grade_ID = '" . $syain_yakusyoku . "'
                WHERE m.member_ID = " . $syain_id;

  try{
    $sql = $pdo->prepare($query_str);
    $sql->execute();
  }catch(PDOException $e){
    print $e->getMessage();
  }

  // echo $query_str;

  $url = "detail01.php?member_ID=" . $syain_id;
  header('Location: ' . $url);
  exit;
?>
