<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>新規社員情報登録</title>
    <link rel="stylesheet" type="text/css" href="./include/common.css">
    <script src="include/functions.js"></script>
  </head>
  <body id="index">
    <?php
      include("./include/header.php");
      include("./include/statics.php");
      include("./include/functions.php");
      include("./include/bootstrap.php");
    ?>
    <form method='post' name="entry" action='./entry02.php'>
      <table class="table table-bordered" id="resulttable">
        <tr>
          <th id="midasi2">名前</th>
          <td><input type="text" maxlength="30" name="syain_name"></td>
        </tr>
        <tr>
          <th id="midasi2">出身地</th>
          <td>
            <select name="pref">
              <option value="" selected>都道府県</option>
              <?php
                foreach($pref_array as $key => $value){
                  echo "<option value='" . $key . "'>" . $value . "</option>";
                }
              ?>
            </select>
          </td>
        </tr>
        <tr>
          <th id="midasi2">性別</th>
          <td>
            <label><input type="radio" name="new_seibetu" value="0" checked>男　</label>
            <label><input type="radio" name="new_seibetu" value="1">女　</label>
            <!-- ↓ foreach でも回せる！！
              foreach($gender_array as $key => $value){
              echo "<input type='radio' value='" . $key . "'>" . $value;
            } -->
          </td>
        </tr>
        <tr>
          <th id="midasi2">年齢</th>
          <td><input type="number" name="age" min="0" value="" id="age">才</td>
        </tr>
        <tr>
          <th id="midasi2">所属部署</th>
          <td>
            <?php
              $result_section = commonSM();
              foreach($result_section as $each){
                if($each['ID'] == 1){
                  echo "<label><input type='radio' name='new_busyo' value='" . $each['ID'] . "' checked>" . $each['section_name'] . "</label>　";
                }else{
                  echo "<label><input type='radio' name='new_busyo' value='" . $each['ID'] . "'>" . $each['section_name'] . "</label>　";
                }
              }
            ?>
          </td>
        </tr>
        <tr>
          <th id="midasi2">役職</th>
          <td>
            <?php
              $result_grade = commonGM();
              foreach($result_grade as $each){
                if($each['ID'] == 1){
                  echo "<label><input type='radio' name='new_yakusyoku' value='" . $each['ID'] . "' checked>" . $each['grade_name'] . "</label>　";
                }else{
                  echo "<label><input type='radio' name='new_yakusyoku' value='" . $each['ID'] . "'>" . $each['grade_name'] . "</label>　";
                }
              }
            ?>
          </td>
        </tr>
      </table>
      <div id="botan">
        <input type="button" class="btn btn-outline-info" value="登録" onclick="goEnt();">
        <input type="reset" class="btn btn-outline-info" value="リセット">
      </div>
    </form>
  </body>
</html>
