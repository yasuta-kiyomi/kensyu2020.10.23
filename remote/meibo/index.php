<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>社員一覧</title>
    <link rel="stylesheet" type="text/css" href="./include/common.css">
    <script type="text/javascript">
      function clearForm(){
        document.search.syain_name.value = "";
        document.search.seibetu.value = "";
        document.search.busyo_name.value = "";
        document.search.yakusyoku_name.value = "";
      }
    </script>
  </head>
  <body id="index">
    <?php
      include("./include/header.php");
      include("./include/functions.php");
      include("./include/bootstrap.php");

      $pdo = commonDB();

      $syain_value = "";
      if(isset($_GET['syain_name']) AND $_GET['syain_name'] != ""){
         $syain_value = $_GET['syain_name'];
      }

      $seibetu_value = "";
      if(isset($_GET['seibetu']) AND $_GET['seibetu'] != ""){
         $seibetu_value = $_GET['seibetu'];
      }

      $busyo_value = "";
      if(isset($_GET['busyo_name']) AND $_GET['busyo_name'] != ""){
         $busyo_value = $_GET['busyo_name'];
      }

      $yakusyoku_value = "";
      if(isset($_GET['yakusyoku_name']) AND $_GET['yakusyoku_name'] != ""){
         $yakusyoku_value = $_GET['yakusyoku_name'];
      }
    ?>
    <form method='get' name="search" action='./index.php' id="kensaku">
      <div><b>名前：</b><input type="text" maxlength="30" name="syain_name" value="<?php echo $syain_value; ?>"></div>
      <div id="purudaun">
        <b>性別：</b>
        <select name="seibetu">
          <option value="" <?php if($seibetu_value == ""){ echo "selected"; } ?>>すべて</option>
          <option value="0" <?php if($seibetu_value == "0"){ echo "selected"; } ?>>男</option>
          <option value="1" <?php if($seibetu_value == "1"){ echo "selected"; } ?>>女</option>
        </select>
        <b>部署：</b>
        <select name="busyo_name">
          <option value="">すべて</option>
          <?php
            $result_section = commonSM();
            foreach($result_section as $each){
              if($each['ID'] == $busyo_value){
                echo "<option value='" . $each['ID'] . "' selected>" . $each['section_name'] . "</option>";
              }else{
                echo "<option value='" . $each['ID'] . "'>" . $each['section_name'] . "</option>";
              }
            }
          ?>
        </select>
        <b>役職：</b>
        <select name="yakusyoku_name">
          <option value="">すべて</option>
          <?php
            $result_grade = commonGM();
            foreach($result_grade as $each){
              if($each['ID'] == $yakusyoku_value){
                echo "<option value='" . $each['ID'] . "' selected>" . $each['grade_name'] . "</option>";
              }else{
                echo "<option value='" . $each['ID'] . "'>" . $each['grade_name'] . "</option>";
              }
            }
          ?>
        </select>
      </div>
      <div id="botan">
        <input type="submit" value="検索" class="btn btn-outline-secondary">
        <input type="button" value="リセット" class="btn btn-outline-secondary" onclick="clearForm();">
      </div>
    </form>
    <hr/>
    <?php
      $query_str = "SELECT
                      m.member_ID,
                      m.name,
                      m.seibetu,
                      sm.section_name,
                      gm.grade_name
                    FROM member AS m
                    LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                    LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                    WHERE 1=1
                   ";

      if($syain_value != ""){  //phpパラメーターチェック
         $query_str .= "AND m.name LIKE '%" . $syain_value . "%'";
      }
      // 元はこれ↓
      // if(isset($_GET['syain_name']) AND $_GET['syain_name'] != ""){
      //    $query_str .= "AND m.name LIKE '%" . $GET['syain_name'] . "%'";
      // }
      if($seibetu_value != ""){
         $query_str .= "AND m.seibetu = " . $seibetu_value;
      }
      if($busyo_value != ""){
         $query_str .= " AND m.section_ID = " . $busyo_value;
      }
      if($yakusyoku_value != ""){
         $query_str .= " AND m.grade_ID = " . $yakusyoku_value;
      }

      // echo $query_str;
      $sql = $pdo->prepare($query_str);
      $sql->execute();
      $result = $sql->fetchAll();
    ?>
    <br/>
    <div id="resulttable">検索結果：
      <?php
        $result_number = count($result);
        echo $result_number;
        // echo count($result);でもOK！
      ?>
    </div>
    <table class="table table-bordered" id="resulttable">
      <tr id="midasi">
        <th id="syain_id">社員ID</th>
        <th id="namae">名前</th>
        <th id="busyo">部署</th>
        <th id="yakusyoku">役職</th>
      </tr>
      <?php
        if($result_number != 0){
          foreach($result as $each){
            echo
              "<tr>" .
                "<td id='syain_id'>" . $each['member_ID'] . "</td>" .
                "<td id='namae'><a href='./detail01.php?member_ID=" . $each['member_ID'] . "'>" . $each['name'] . "</a></td>" .
                "<td id='busyo'>" . $each['section_name'] . "</td>" .
                "<td id='yakusyoku'>" . $each['grade_name'] . "</td>" .
              "</tr>";
          }
        }else{
          echo "<tr><td colspan='4' id='nasi'>検索結果なし</td></tr>";
        }
      ?>
    </table>
  </body>
</html>
