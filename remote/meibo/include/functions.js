function goEnt(){
  // window.alert('ようこそ。goEntですよ。');
  // window.alert(document.entry.new_syain.value); ⇒（名前欄に）入力した値がダイアログに表示される！

  var new_name = document.entry.syain_name.value; // 名前を統一しよー。
  new_name = new_name.replace(/\s+/g,""); // 「~.replace(1,2):1があったら、2に書き換えるよ－」というやつ。
  document.entry.syain_name.value = new_name;
  if(new_name == ""){ // 未入力の時に、エラー文言を出す。
    window.alert('名前は必須です');
    return false; // 返値を返している！「ここで処理が終わりです」ということ。
  }

  if(document.entry.pref.value == ""){
    window.alert('都道府県は必須です');
    return false;
  }

  var age = document.entry.age.value;
  var regex = new RegExp(/^[0-9]+$/);
  if(age == "" || age > 99 || age < 1 || !regex.test(age)){ // 境界値
    window.alert('年齢は必須です/1-99の範囲で入力してください/整数で入力してください');
    return false;
  // }else if(document.entry.new_age.value > 100 ) {
  //   window.alert('1-99の範囲で入力してください');
  //   return false;
  // }else if(document.entry.new_age.value < 0){
  //   window.alert('整数で入力してください');
  //   return false;
  // あんまり「elseif」続けるのもややこしいよね…。
  // 「number」にしているから、文字が入っても大丈夫だと思われ。
  // ※「e」が入っても、登録はできないしいいかな？
  }

  if(window.confirm('更新を行います。よろしいですか？')){
    document.entry.submit();
  }
}
