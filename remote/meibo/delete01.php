<!DOCTYPE html>
<?php
  include("./include/functions.php");

  $pdo = commonDB();

  $syain_id = "";
  if(isset($_POST['member_ID']) AND $_POST['member_ID'] != ""){
     $syain_id = $_POST['member_ID'];
  }else{
    commonError();
  }

  $query_str = "DELETE FROM member
                WHERE member.member_ID = " . $syain_id;

  try{
    $sql = $pdo->prepare($query_str);
    $sql->execute();
  }catch(PDOException $e){
    print $e->getMessage();
  }

  // echo $query_str;

  $url = "index.php";
  header('Location: ' . $url);
  exit;
?>
