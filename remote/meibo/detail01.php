<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>社員情報詳細</title>
    <link rel="stylesheet" type="text/css" href="./include/common.css">
    <script type="text/javascript">
      function goDel(){
        if(window.confirm('削除を行います。よろしいですか？')){
          document.delete.submit();
          // 成功処理の時、ドキュメント（htmlページ全て）の「delete」というところは「submit」しよう！みたいな…？
        }
      }
    </script>
  </head>
  <body id="index">
    <?php
      include("./include/header.php");
      include("./include/statics.php");
      include("./include/functions.php");
      include("./include/bootstrap.php");

      $pdo = commonDB();

      $query_str = "SELECT
                      m.member_ID,
                      m.name,
                      m.pref,
                      m.seibetu,
                      m.age,
                      sm.section_name,
                      gm.grade_name
                    FROM member AS m
                    LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                    LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                   ";

      if(isset($_GET['member_ID']) AND $_GET['member_ID'] != "" AND is_numeric($_GET['member_ID'])){
      // 条件：➀メンバーIDが存在するか ➁メンバーIDに値が入力されているか ➂入力されている値が数字かどうか みたいな
        $query_str .= " WHERE m.member_ID = " . $_GET['member_ID'];
      }else{
        commonError();
      }
      // echo $query_str;
      $sql = $pdo->prepare($query_str);
      $sql->execute();
      $result = $sql->fetchAll();

      if(count($result) != 1){
        commonError();
      }
    ?>
    <table class="table table-bordered" id="resulttable">
      <tr>
        <th id="midasi2">社員ID</th>
        <td><?php echo $result[0]['member_ID']; ?></td>
      </tr>
      <tr>
        <th id="midasi2">名前</th>
        <td><?php echo $result[0]['name']; ?></td>
      </tr>
      <tr>
        <th id="midasi2">出身地</th>
        <td><?php echo $pref_array[$result[0]['pref']]; ?></td>
      </tr>
      <tr>
        <th id="midasi2">性別</th>
        <td><?php echo $gender_array[$result[0]['seibetu']]; ?></td>
      </tr>
      <tr>
        <th id="midasi2">年齢</th>
        <td><?php echo $result[0]['age']; ?></td>
      </tr>
      <tr>
        <th id="midasi2">所属部署</th>
        <td><?php echo $result[0]['section_name']; ?></td>
      </tr>
      <tr>
        <th id="midasi2">役職</th>
        <td><?php echo $result[0]['grade_name']; ?></td>
      </tr>
    </table>
    <div id="botan2">
      <form method="get" action="./entry_update01.php">
        <input type="hidden"  name="member_ID" value="<?php echo $result[0]['member_ID']; ?>" id="botan">
        <input type="submit" class="btn btn-outline-info" value="編集">
      </form>
      <form method="post" name="delete" action="./delete01.php">
        <input type="hidden" name="member_ID" value="<?php echo $result[0]['member_ID']; ?>" id="botan">
        <input type="button" class="btn btn-outline-info" value="削除" onclick="goDel();">
        <!-- type="submit"だと、キャンセルの時にも動いてしまうため。 -->
      </form>
    </div>
  </body>
</html>
