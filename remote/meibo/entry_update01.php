<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>既存社員情報修正</title>
    <link rel="stylesheet" type="text/css" href="./include/common.css">
    <script src="include/functions.js"></script>
  </head>
  <body id="index">
    <?php
      include("./include/header.php");
      include("./include/statics.php");
      include("./include/functions.php");
      include("./include/bootstrap.php");

      $pdo = commonDB();

      $query_str = "SELECT
                      m.member_ID,
                      m.name,
                      m.pref,
                      m.seibetu,
                      m.age,
                      m.section_ID,
                      m.grade_ID
                    FROM member AS m
                   ";

      if(isset($_GET['member_ID']) AND $_GET['member_ID'] != ""){
        $query_str .= " WHERE m.member_ID = " . $_GET['member_ID'];
      }else{
        commonError();
      }

      // echo $query_str;
      $sql = $pdo->prepare($query_str);
      $sql->execute();
      $result = $sql->fetchAll();

      if(count($result) != 1){
        commonError();
      }
    ?>
    <form method='post' name="entry" action='./entry_update02.php'>
      <table class="table table-bordered" id="resulttable">
        <tr>
          <th id="midasi2">社員ID</th>
          <td><?php echo $result[0]['member_ID']; ?></td>
        </tr>
        <tr>
          <th id="midasi2">名前</th>
          <td><input type="text" name="syain_name" maxlength="30" value="<?php echo $result[0]['name']; ?>"></td>
        </tr>
        <tr>
          <th id="midasi2">出身地</th>
          <td>
            <select name="pref">
              <?php
                foreach($pref_array as $key => $value){
                  if($key == $result[0]['pref']){
                  // ～ == $pref_array[$result[0]['pref']] だと、配列の数字ではなく値（～県など）を表していた…。
                    echo "<option value='" . $key . "' selected>" . $value . "</option>";
                  }else{
                    echo "<option value='" . $key . "'>" . $value . "</option>";
                  }
                }
              ?>
            </select>
          </td>
        </tr>
        <tr>
          <th id="midasi2">性別</th>
          <td>
            <label><input type="radio" name="up_seibetu" value="0" <?php if($result[0]['seibetu'] == "0"){ echo "checked"; } ?>>男　</label>
            <label><input type="radio" name="up_seibetu" value="1" <?php if($result[0]['seibetu'] == "1"){ echo "checked"; } ?>>女</label>
          </td>
        </tr>
        <tr>
          <th id="midasi2">年齢</th>
          <td><input type="number" name="age" min="0" value="<?php echo $result[0]['age']; ?>" id="age">才</td>
        </tr>
        <tr>
          <th id="midasi2">所属部署</th>
          <td>
            <?php
              $result_section = commonSM();
              foreach($result_section as $each){
                if($each['ID'] == $result[0]['section_ID']){
                  echo "<label><input type='radio' name='up_busyo' value='" . $each['ID'] . "' checked>" . $each['section_name'] . "</label>　";
                }else{
                  echo "<label><input type='radio' name='up_busyo' value='" . $each['ID'] . "'>" . $each['section_name'] . "</label>　";
                }
              }
            ?>
          </td>
        </tr>
        <tr>
          <th id="midasi2">役職</th>
          <td>
            <?php
              $result_grade = commonGM();
              foreach($result_grade as $each){
                if($each['ID'] == $result[0]['grade_ID']){
                  echo "<label><input type='radio' name='up_yakusyoku' value='" . $each['ID'] . "' checked>" . $each['grade_name'] . "</label>　";
                }else{
                  echo "<label><input type='radio' name='up_yakusyoku' value='" . $each['ID'] . "'>" . $each['grade_name'] . "</label>　";
                }
              }
            ?>
          </td>
        </tr>
      </table>
      <div id="botan">
        <input type="hidden" name="member_ID" value="<?php echo $result[0]['member_ID']; ?>">
        <input type="button" class="btn btn-outline-info" value="登録" onclick="goEnt();">
        <input type="reset" class="btn btn-outline-info" value="リセット">
      </div>
    </form>
  </body>
</html>
