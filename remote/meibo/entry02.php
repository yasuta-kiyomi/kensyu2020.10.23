<!DOCTYPE html>
<?php
  include("./include/statics.php");
  include("./include/functions.php");

  $pdo = commonDB();

  $new_name = "";
  if(isset($_POST['syain_name']) AND $_POST['syain_name'] != ""){
     $new_name = $_POST['syain_name'];
  }else{
    commonError();
  }

  $new_pref = "";
  if(isset($_POST['pref']) AND $_POST['pref'] != ""){
     $new_pref = $_POST['pref'];
  }else{
    commonError();
  }

  $new_seibetu = "";
  if(isset($_POST['new_seibetu']) AND $_POST['new_seibetu'] != ""){
     $new_seibetu = $_POST['new_seibetu'];
  }else{
    commonError();
  }

  $new_age = "";
  if(isset($_POST['age']) AND $_POST['age'] != "" AND is_numeric($_POST['age'])){
     $new_age = $_POST['age'];
  }else{
    commonError();
  }

  $new_busyo = "";
  if(isset($_POST['new_busyo']) AND $_POST['new_busyo'] != ""){
     $new_busyo = $_POST['new_busyo'];
  }else{
    commonError();
  }

  $new_yakusyoku = "";
  if(isset($_POST['new_yakusyoku']) AND $_POST['new_yakusyoku'] != ""){
     $new_yakusyoku = $_POST['new_yakusyoku'];
  }else{
    commonError();
  }

  $query_str = "INSERT INTO member (member_ID, name, pref, seibetu, age, section_ID, grade_ID)
                VALUES (NULL,
                   '" . $new_name . "',
                   '" . $new_pref . "',
                   '" . $new_seibetu . "',
                   '" . $new_age . "',
                   '" . $new_busyo . "',
                   '" . $new_yakusyoku . "')";
                // ;つけるなら""の外ね！！

  try{
    $sql = $pdo->prepare($query_str);
    $sql->execute();

    $new_id = $pdo->lastInsertId('member_ID');

  }catch(PDOException $e){
    print $e->getMessage();
  }

  // echo $query_str;

  $url = "detail01.php?member_ID=" . $new_id;
  header('Location: ' . $url);
  exit;
?>
