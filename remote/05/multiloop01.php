<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>二次元配列</title>
  </head>
  <body>
    <?php
      $team_a = array("今永","濱口","平良","石田","東");
      $team_b = array("伊藤","高城","嶺井","戸柱","益子");
      $team_c = array("宮崎","柴田","大和","石川","倉本");
      $team_d = array("佐野","乙坂","細川","梶谷","神里");
      $team_e = array("三島","三上","国吉","砂田","武藤");

      $team_all = array($team_a, $team_b, $team_c, $team_d, $team_e);

      echo "<pre>";
      var_dump($team_all);
      echo "</pre>";

      echo "<hr/>";

      echo "<table border='1'>";
      foreach ($team_all as $team) {
        echo "<tr>";
        foreach ($team as $value) {
          echo "<td>" . $value . "</td>";
        }
        echo "</tr>";
      }
      echo "</table>";
    ?>
  </body>
</html>
