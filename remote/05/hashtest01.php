<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>連想配列</title>
  </head>
  <body>
    <?php
      $me_data = array(
        'fruit' => 'もも',
        'sport' => 'バレーボール',
        'town' => '福島',
        'age' => 21,
        'food' => 'オムライス'
        );

      echo $me_data['town'] . "<br/>"; // 横浜 と表示される

      echo $me_data['age']; // 21 と表示される

      echo $me_data['school']; // エラーになる

      $me_data['age'] = 25; // 上書きされる

      var_dump($me_data); // 配列の中身がすべて表示される

      echo "<hr/>";
      foreach($me_data as $each){
        echo $each . "<br/>";
      }
      echo "<hr/>";
      foreach($me_data as $key => $value){
        echo $key . " : " . $value . "<br/>";
      }
      echo "<hr/>";
    ?>
    <pre>
      <?php var_dump($me_data); ?>
    </pre>
  </body>
</html>
