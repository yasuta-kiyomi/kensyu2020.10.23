<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>データ構造</title>
  </head>
  <body>
    <?php
      $character = array(
        array(
          "id" => "01",
          "name" => "安田",
          "from" => "福島",
          "age" => "21",
        ),
        array(
          "id" => "02",
          "name" => "柳沼",
          "from" => "宮城",
          "age" => "22",
        ),
        array(
          "id" => "03",
          "name" => "相原",
          "from" => "茨城",
          "age" => "25",
        ),
      );
      var_dump($character);
    ?>
    <hr>
    <table border="1">
      <tr>
        <th>番号</th>
        <th>名前</th>
        <th>出身地</th>
        <th>年齢</th>
      </tr>
      <?php
        foreach($character as $each){
          echo
            "<tr>" .
              "<td>" . $each['id'] . "</td>" .
              "<td>" . $each['name'] . "</td>" .
              "<td>" . $each['from'] . "</td>" .
              "<td>" . $each['age'] . "</td>" .
            "</tr>";
        }
      ?>
    </table>
  </body>
</html>
